# dotfiles

This is a repository with my configuration files.

## Introduction

## Content

### VIM

My VIM configuration uses the theme *gruvbox* (morhetz/gruvbox).

I use [vim-plug]( https://github.com/junegunn/vim-plug) to install plugins.

Following plugins are used:
* ~~nerdtree~~ now using *netrw*
* goyo
* limelight
* tabular
* vim-markdown
* vim-instant-markdown
* calendar-vim
* vimwiki
* vim-vinegar