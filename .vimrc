"
"                 __   _(_)_ __ ___  _ __ ___
"                 \ \ / / | '_ ` _ \| '__/ __|
"                  \ V /| | | | | | | | | (__
"                   \_/ |_|_| |_| |_|_|  \___|
"
" Author: Thomas Wenzel <utopist@mailbox.org>

set nocompatible
set enc=utf-8

" ----------------------------------------------------------------------
" Plugins (with Plug)
" See: https://github.com/junegunn/vim-plug
" ----------------------------------------------------------------------
call plug#begin()
"Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }
Plug 'junegunn/goyo.vim'
Plug 'junegunn/limelight.vim'
Plug 'godlygeek/tabular'
Plug 'plasticboy/vim-markdown'
Plug 'suan/vim-instant-markdown', {'for': 'markdown'}
Plug 'mattn/calendar-vim'       " calendar works with wiki
Plug 'vimwiki/vimwiki'          " wiki
Plug 'morhetz/gruvbox'          " my favorite colorscheme
Plug 'tpope/vim-vinegar'        " Nice dressing for `:netrw`.

" All of your Plugins must be added before the following line
call plug#end()
syntax enable                   " Enable syntax

set number                      " Show line number
set numberwidth=5
set relativenumber		" Relative numbering
set backspace=indent,eol,start  " Allow backspace in insert mode
set visualbell                  " No sounds
set wildmenu                    " enhanced command completion
set autoread                    " Reload files changed outside vim
set hidden                      " Allow buffers to exist in background
set noswapfile                  " No swap file
set nobackup                    " No backup
set t_Co=256                    " Set terminal color to 16

set scrolloff=8			" Start scrolling when we're 8 lines away from margin
set sidescrolloff=15
set sidescroll=1

set textwidth=80                " Make it obvious where 80 characters is
set colorcolumn=+1             

set cursorline                  " Highlight current line
set completeopt=menuone,preview " Complete option
set laststatus=2

" ----------------------------------------------------------------------
" Tabs ans Linebreaks
" ----------------------------------------------------------------------
set smarttab
set shiftwidth=4
set softtabstop=4
set tabstop=4
set expandtab
set nowrap                     " Do not wrap lines
set linebreak                  " Wrap lines at convenient point
set nojoinspaces               " Use one space after punctuation, not two.

syntax on                       " Turn on syntax highlighting
syntax enable                   " Enable syntax
autocmd FocusLost * silent! wa  " Automatically save file

" Folding
" set nofoldenable      " Auto fold code
set foldmethod=syntax " Fold are defined by syntax highlighting

" Indentation
" Don't enable smarindent or cindent with filetype plugin indent on
filetype plugin indent on   " Indentation based on filetype
set autoindent              " Does not interfere with other indentation settings

" Invisible characters
set list listchars=tab:»»,trail:•,nbsp:~     " Display invisible characters

" Mouse
set mouse=a

" morhetz/gruvbox
let g:gruvbox_italic=1
let g:gruvbox_improved_strings=1
let g:gruvbox_improved_warnings=1
let g:gruvbox_guisp_fallback='fg'
let g:gruvbox_contrast_light='hard'
let g:gruvbox_contrast_dark='medium'
set background=dark
colorscheme gruvbox

if has('termguicolors')
    let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
    let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
    set termguicolors
endif

if has("gui_running")
	set guifont=Consolas:h11:cDEFAULT
	set guioptions-=m  "remove menu bar
    set guioptions-=T  "remove toolbar
    set guioptions-=r  "remove right-hand scroll bar
    set guioptions-=L  "remove left-hand scroll bar
    set lines=40 columns=122
endif

"status line: modifiedflag, charcount, filepercent, filepath
set statusline=%=%m\ %c\ %P\ %f

" ----------------------------------------------------------------------
" vimwiki/vimwiki
" ----------------------------------------------------------------------
let g:vimwiki_list = [{'path': '~/vimwiki/', 'syntax': 'markdown', 'ext':'.md'}]

" ----------------------------------------------------------------------
" netrw
" ----------------------------------------------------------------------
let g:netrw_banner=0         " Disable banner
let g:netrw_browse_split = 4 " Open in prior window
let g:netrw_altv = 1         " Open split to the right
let g:netrw_liststyle = 3    " Tree view
let g:netrw_winsize = 20
let g:netrw_list_hide=netrw_gitignore#Hide()
let g:netrw_list_hide.=',\(^\|\s\s\)\zs\.\S\+'
let g:netrw_localrmdir='rm -r'


" ----------------------------------------------------------------------
" Remaps
" ----------------------------------------------------------------------

" make the leader the same key, and make it close
let mapleader=','
let maplocalleader=','

nmap <C-n> :Lexplore<CR>

nnoremap <Leader>gy :Goyo<CR>
autocmd! User GoyoEnter Limelight       " if Goyo starts, Limelight starts too
autocmd! User GoyoLeave Limelight!      " if Goyo stops, Limelight stops too